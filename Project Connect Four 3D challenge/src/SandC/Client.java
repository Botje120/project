package SandC;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;

import game.Board;
import game.ComputerPlayer;
import game.HumanPlayer;
import game.Mark;
import game.Player;

/**
 * The Class Client.
 */
public class Client implements Protocol {

	/** The tui. */
	public static TUI tui;

	/** The Constant USAGE. */
	private static final String USAGE = "usage: <address> <portnumber>";

	/** The portnumber. */
	private static int portnumber;

	/** The Constant NUMBER_PLAYERS. */
	private static final int NUMBER_PLAYERS = 2;

	/** The players. */
	private ArrayList<Player> players;

	/** The board. */
	public Board board;

	/** The nickname. */
	private String nickname;

	/** The you. */
	private Player you;

	/** The other. */
	private Player other;

	/** The in. */
	private BufferedReader in;

	/** The out. */
	private BufferedWriter out;

	/** The playcur. */
	private Player playcur;

	/** The current. */
	private int current;
	
	/** The move. */
	private String move;
	
	/** The thinkingtime. */
	private int thinkingtime = 30000;

	/**
	 * Instantiates a new client.
	 */
	public Client() {
		Client.tui = new TUI(this);
		Board board = new Board();
		board.addObserver(tui);
		this.board = board;
		current = 0;
	}

	/**
	 * Start.
	 */
	private void start() {
		System.out.print("Give address and portnumber, divide them by a space");
		Scanner inppput = new Scanner(System.in);
		String input = inppput.nextLine();
		String[] ar = input.split(" ");
		if (ar.length != 2) {
			System.out.println(USAGE);
			System.exit(0);
		}
		tui = new TUI(this);
		this.nickname = tui.askName();

		InetAddress addr = null;
		Socket sock = null;

		// check args[0] - the IP-adress
		try {
			addr = InetAddress.getByName(ar[0]);
		} catch (UnknownHostException e) {
			System.out.println(USAGE);
			System.out.println("ERROR: host " + ar[0] + " unknown");
			System.exit(0);
		}

		portnumber = Integer.parseInt(ar[1]);

		// try to open a Socket to the server
		try {
			sock = new Socket(addr, portnumber);
		} catch (IOException e) {
			System.out.println("ERROR: could not create a socket on " + addr + " and port " + portnumber);
		}

		if (tui.askComputerPlayerOrHumanPlayer()) {
			players = new ArrayList<Player>();
			you = new HumanPlayer("you", Mark.RED);
			other = new HumanPlayer("other", Mark.YELLOW);

			players.add(you);
			players.add(other);
		} else {
			setThinkingtime();
			players = new ArrayList<Player>();
			you = new ComputerPlayer(Mark.RED);
			other = new ComputerPlayer(Mark.YELLOW);

			players.add(you);
			players.add(other);
		}

		try {
			in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));

			sendMsg(CLIENT_JOINREQUEST + " " + nickname);

			String msg;
			boolean continu = true;
			try {
				while (continu) {
					msg = in.readLine();
					while (msg != null && continu) {
						if (msg.equals(SERVER_CONNECTIONLOST)) {
							continu = false;
						}
						handleCommand(msg, out, in);
						try{
							msg = in.readLine();
						}
						catch (java.io.IOException e){
							tui.outPutConnectionServerLost();
							in.close();
							out.close();
							continu = false;
						}
					}
				}
				try {
					sock.close();
				} catch (IOException e) {
					System.out.println("ERROR: unable to close the socket");
					e.printStackTrace();
				}
			} catch (IOException e) {
				System.out.println("ERROR: not able to read the whole time");
				e.printStackTrace();
			}

		} catch (IOException e) {
			System.out.println("ERROR: unable to communicate to server");
			e.printStackTrace();
		}
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		Client client = new Client();
		client.start();
	}

	/**
	 * Handle command.
	 *
	 * @param msg
	 *            the msg
	 * @param out
	 *            the out
	 * @param in
	 *            the in
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	/*
	 * @ requires msg != null; requires out != null; requires in != null;
	 */
	public void handleCommand(String msg, BufferedWriter out, BufferedReader in) throws IOException {
		if (msg.startsWith(SERVER_ACCEPTREQUEST)) {
			tui.outPutAcceptRequest();
			if (tui.readyToPlayGame()) {
				sendMsg(CLIENT_GAMEREQUEST);
			} else {
				try {
					boolean yeah = true;
					while (yeah) {
						Thread.sleep((long) 1);
						if (tui.readyToPlayGame()) {
							sendMsg(CLIENT_GAMEREQUEST);
							yeah = false;
						}
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}

		else if (msg.startsWith(SERVER_DENYREQUEST)) {
			tui.outPutDeniedRequest();
			tui.askName();
			sendMsg(CLIENT_JOINREQUEST + " " + tui.getNickname() + " " + 0 + " " + 0 + " " + 0 + " " + 0);
		}

		else if (msg.startsWith(SERVER_STARTGAME)) {
			tui.outPutStartGame();
		}

		else if (msg.startsWith(SERVER_MOVEREQUEST)) {
			String[] ar = msg.split(" ");
			if (ar[1].equals(nickname)) {
				if (players.get(0) instanceof HumanPlayer) {		
						tuiaskMove();
						String[] ar2 = move.split(" ");
						int x = Integer.parseInt(ar2[0]);
						int z = Integer.parseInt(ar2[1]);
						int y = board.calculateY(x, z);
						sendMsg(CLIENT_SETMOVE + " " + move);
					
				} else { // Thus players.get(0) is a computerplayer
					try {
						Thread.sleep((long) getThinkingtime());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					ArrayList<Integer> coordinates = players.get(0).determineMove(board);
					int x = coordinates.get(0);
					int z = coordinates.get(1);
					int y = board.calculateY(x, z);
					String move = "" + x + " " + z;
					sendMsg(CLIENT_SETMOVE + " " + move);
				}

			}

		}

		else if (msg.equals(SERVER_DENYMOVE)) {
			tui.outPutDeniedMove();
			tuiaskMove();
			while (move == null) {
				tui.outPutDeniedMove();
				tuiaskMove();
			}
			sendMsg(CLIENT_SETMOVE + " " + move);
		}

		else if (msg.startsWith(SERVER_NOTIFYMOVE)) {
			String[] ar = msg.split(" ");
			int x = Integer.parseInt(ar[2]);
			int z = Integer.parseInt(ar[3]);
			int y = Integer.parseInt(ar[4]);
			if (ar[1].equals(nickname)){
				board.setField(x, z, y, Mark.RED);
			}
			else{
				board.setField(x,z,y, Mark.YELLOW);
			}
			
		}

		else if (msg.startsWith(SERVER_CONNECTIONLOST)) {
			String[] ar = msg.split(" ");
			if (ar[1].equals(tui.getNickname())) {
				tui.outPutYourConnectionLost();
			} else {
				tui.outPutOtherConnectionLost();
			}
		}

		else if (msg.equals(SERVER_INVALIDCOMMAND)) {
			tui.outPutInvalidCommand();
		}

		else if (msg.startsWith(SERVER_GAMEOVER)) {
			String[] ar = msg.split(" ");
			String nameWinner = ar[1];
			if (nameWinner == null){
				tui.outPutDraw();
			}
			else if (nameWinner.equals(tui.getNickname())) {
				tui.outPutYouWin();
			}
			else {
				tui.outPutYouLost();
			}
			if (tui.readBoolean("\n> Play another time? (yes/no)?", "yes", "no")) {
				sendMsg(CLIENT_GAMEREQUEST);
			}
		}

		else if (msg.equals(SERVER_WAITFORCLIENT)) {
			tui.waitOpponent();
		}

		else {

		}

	}


	/**
	 * Send msg.
	 *
	 * @param msg
	 *            the msg
	 */
	//@requires msg!= null;
	public void sendMsg(String msg) throws java.net.SocketException {
		if (out != null) {
			try {
				System.out.println(msg);
				out.write(msg);
				out.newLine();
				out.flush();
			} 
			catch (java.net.SocketException s){
				tui.outPutConnectionServerLost();
			}
			catch (IOException e) {
				e.printStackTrace();
				tui.outPutConnectionServerLost();
			}
		}
	}
	
	/**
	 * Tuiask move.
	 */
	public void tuiaskMove(){
		try {
			move = tui.askMove();
		}
		catch(NumberFormatException n){
			tuiaskMove();
		}
	}

	public int getThinkingtime() {
		return thinkingtime;
	}

	public void setThinkingtime() {
		thinkingtime = tui.setTime();
	}
}

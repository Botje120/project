package SandC;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import game.Board;
import game.HumanPlayer;
import game.Mark;
import game.Player;
import game.Strategy;
import SandC.GameController;
import SandC.Server;

/**
 * The Class ClientHandler.
 */
public class ClientHandler extends Player implements Runnable, Protocol {
	
	/** The in. */
	private BufferedReader in;
	
	/** The out. */
	private BufferedWriter out;
	
	/** The sock. */
	private Socket sock;
	
	/** The nick namesready to play. */
	protected ArrayList <String> nickNamesreadyToPlay = new ArrayList<String>();
	
	/** The nickname 2. */
	private String nickname2;
	
	/** The move. */
	protected String move;
	
	/** The player. */
	private Player player;
	
	/** The server. */
	private Server server;
	
	/** The game. */
	private GameController game;
	
	/** The exit. */
	public final String EXIT = "exit";
	
	/** The winner. */
	private String winner;
	
	private Player playcur;


	/**
	 * Instantiates a new client handler.
	 *
	 * @param server the server
	 * @param sock the sock
	 * @param nickname the nickname
	 * @param m the m
	 * @param strategy the strategy
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public ClientHandler(Server server, Socket sock, String nickname, Mark m, Strategy strategy) throws IOException {
		super(nickname, m);
		in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
		out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
		this.sock = sock;
		this.server = server;
		player = new HumanPlayer(nickname, m);

	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		String msg;
		boolean continu = true;
		try {
			while (continu){
				msg = in.readLine();
				while (msg != null) {
					if (msg.equals(EXIT)){
						continu = false;
					}
					try {
						handleCommand(msg, out, in);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					out.newLine();
					out.flush();
					msg = in.readLine();
				}
			}
			shutdown();
		} catch (IOException e) {
			// For now, ignore and let thread stop.
		}

	}

	/**
	 * Sets the game of the ClientHandlers.
	 *
	 * @param game the new game
	 */
	public void setGame(GameController game){
		this.game = game;
	}


	/**
	 * Handle commands from the Client the ClientHandler is related too.
	 *
	 * @param msg            command from client
	 * @param out            Writer to to write the result to.
	 * @param in the in
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	private void handleCommand(String msg, BufferedWriter out, BufferedReader in) throws IOException, InterruptedException {
		if (msg.startsWith(CLIENT_JOINREQUEST)) {
			System.out.println("Got the JoinRequest");
			String[] ar = msg.split(" ");
			nickname2 = ar[1];
			player.setName(nickname2);
			this.setName(nickname2);
			if (server.joinrequest(nickname2, this)){
				System.out.println("Sent acceptrequest");
				this.sendMsg(SERVER_ACCEPTREQUEST + " " + ar[1] + " " +  0 + " " + 0 + " " +  0 + " " + 0);
			}
			else {
				this.sendMsg(SERVER_DENYREQUEST + " " + nickname2);
			}
		}

		else if (msg.equals(CLIENT_GAMEREQUEST)) {
			if (server.moveClientToReady(this)){
				if (!game.equals(null)){
					System.out.println("test");
					broadCast(SERVER_STARTGAME + " " + game.players[0].getName() + " " + game.players[1].getName());
					System.out.println("Sent startgame");
					broadCast(SERVER_MOVEREQUEST + " " + (game.players[game.current]).getName());
				}
				else{
					System.out.println("Client is not in game");
				}

			}
			else {
				this.sendMsg(SERVER_WAITFORCLIENT);
			}


		} 
		else if (msg.startsWith(CLIENT_SETMOVE)){
			String[] ar = msg.split(" ");
			int x = Integer.parseInt(ar[1]);
			int z = Integer.parseInt(ar[2]);
			int y = game.board.calculateY(x, z);
			if (game.makeMoves(x, z, y)){
				broadCast(SERVER_NOTIFYMOVE + " " + nickname2 + " " + x + " " + z  + " " + y );
				System.out.println("notified move");
				if (game.CheckGameOver()){
					String winner = game.Winner();
					broadCast(SERVER_GAMEOVER + " " + winner);
					server.endGame(game);
				}


				else{
						playcur = game.players[game.current];
						broadCast(SERVER_MOVEREQUEST + " " + playcur.getName());
				}
			}
			else{
				this.sendMsg(SERVER_DENYMOVE);
			}

		}
		else if (msg.equals(EXIT)){
			server.inGameMap.remove(nickname2);
			server.joinedMap.remove(nickname2);
			server.readyToPlayMap.remove(nickname2);
		}
		else {
			this.sendMsg(SERVER_INVALIDCOMMAND + " " + msg);
		}
	}

	/**
	 * Shutdowns the socket.
	 */
	// shuts down the socket
	private void shutdown() {
		try {
			sock.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see game.Player#determineMove(game.Board)
	 */
	// determineMove depending on which player it is 
	@Override
	public ArrayList<Integer> determineMove(Board board) {
		return player.determineMove(board);
	}

	/**
	 * Send msg to BufferedWriter out.
	 *
	 * @param msg the msg
	 */
	public void sendMsg(String msg) {
		try{
			System.out.println(msg);
			out.write(msg);
			out.newLine();
			out.flush();
		}
		catch (java.net.SocketException s){
			System.out.println("Game has ended");
			this.server.endGame(game);
		}
		catch (IOException e){
			e.printStackTrace();
		}

	}

	/**
	 * Broadcast the message to all of the clients.
	 *
	 * @param msg the msg
	 */
	public void broadCast(String msg){
		if (game != null){
				game.broadcast(msg);
		}
	}

}

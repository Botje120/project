package SandC;


import game.Board;
import game.Game;
import game.Player;

/**
 * The Class GameController.
 */
public class GameController extends Game implements Runnable {
	
	/** The Constant NUMBER_PLAYERS. */
	public static final int NUMBER_PLAYERS = 2;

	/*@
		       protected invariant board != null;
	 */
	/**
	 * The board.
	 */
	protected Board board;

	/*@
		       protected invariant players.length == NUMBER_PLAYERS;
		       protected invariant (\forall int i; 0 <= i && i < NUMBER_PLAYERS; players[i] != null); 
	 */
	/**
	 * The 2 players of the game.
	 */
	protected Player[] players;

	/*@
		       protected invariant 0 <= current  && current < NUMBER_PLAYERS;
	 */
	/**
	 * Index of the current player.
	 */
	protected int current;


	/*@
		      requires s0 != null;
		      requires s1 != null;
	 */
	/**
	 * Creates a new Game object.
	 * 
	 * @param s0
	 *            the first player
	 * @param s1
	 *            the second player
	 */
	public GameController(Player s0, Player s1) {
		super(s0, s1);
		((ClientHandler) s0).setGame(this);
		((ClientHandler) s1).setGame(this);
		this.board = getBoard();
		this.players = getPlayers();		
	}

	/**
	 * Make moves, but also checks if the move is valid and notify the observers of the board
	 *
	 * @param x the x
	 * @param z the z
	 * @param y the y
	 * @return true, if successful
	 */ 
	//@ requires x < 4 && x >= 0;
	//@ requires z < 4 && z >= 0;
	//@	requires y < 4 && y >= 0;
	public boolean makeMoves(int x, int z, int y){
		if (!board.hasWinner() && !board.isFull() && isValidMove(x, z, y)){
			Player playcur = players[current];
			playcur.makeMove(board, x, z, y);
			current = (++current) % NUMBER_PLAYERS;
			return true;
		}
		return false;
	}
	
	/**
	 * Checks if the game is over.
	 *
	 * @return true, if successful
	 */
	public boolean CheckGameOver(){
		return (board.hasWinner() || board.isFull());
	}
	
	/**
	 * Determines the name of the winner.
	 *
	 * @return A string with the name of the Player who won.
	 */
	// checks who is the winner!
	public String Winner(){
		if (board.hasWinner()){
			if (board.isWinner(players[0].getMark())){
				return players[0].getName();
			}
			else {
				return players[1].getName();
			}
		}
		else {
			return null;
		}

	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	// the run method??
	@Override
	public void run() {
		//never used, so no need to implement

	}

	/**
	 * Ask Y on board.
	 *
	 * @param x the x
	 * @param z the z
	 * @return the int y of the field with the x and z, you put in.
	 */
		//@ requires x < 4 && x >= 0;
		//@ requires z < 4 && z >= 0;
		//@	ensures  \result < 4 && \result >= 0;
	public int askYOnBoard(int x, int z){
		return board.calculateY(x, z);
	}

	/**
	 * Broadcast the message to all players.
	 *
	 * @param msg the msg
	 */
	//@ requires msg != null;
	public void broadcast(String msg){
		for (Player p : players){
			((ClientHandler) p).sendMsg(msg);
		}
	}
	
	/**
	 * Checks if is valid move.
	 *
	 * @param x the x
	 * @param z the z
	 * @param y the y
	 * @return true, if is valid move
	 */
	// decide if the move the client requested is valid!
	public boolean isValidMove(int x, int z, int y){
		return (x >= 0 && x <4 && z >= 0 && z < 4 && y >=0 && y < 4);
	}

}

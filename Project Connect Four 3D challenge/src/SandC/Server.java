package SandC;

import java.io.IOException; 
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import game.Mark;
import game.SmartStrategy;

/**
 * The Class Server.
 */
public class Server extends Thread{

	/** The ssock. */
	private ServerSocket ssock;
	
	/** The joined list. */
	public Map<String, ClientHandler> joinedMap;
	
	/** The Map cli handler name. */
	public Map<String, ClientHandler> MapCliHandlerName = new HashMap<String, ClientHandler>();
	
	/** The ready to play list. */
	public Map<String, ClientHandler> readyToPlayMap;
	
	/** The in game list. */
	public Map<String, ClientHandler> inGameMap;
	
	/** The games. */
	private List<GameController> games = new ArrayList<GameController>();
	
	/** The Computer player yes. */
	private static boolean ComputerPlayerYes;
	
	/** The nickname. */
	private String nickname;

	/**
	 * Instantiates a new server.
	 *
	 * @param ssock the ssock
	 */
	public Server(ServerSocket ssock){
		this.ssock = ssock;
		joinedMap = new HashMap<String, ClientHandler>();
		readyToPlayMap = new HashMap<String, ClientHandler>();
		inGameMap = new HashMap<String, ClientHandler>();
	}
	
	

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		try {
			while (true) {
				Socket sock = ssock.accept();
				System.out.println("Client connected!");
				Thread handler = new Thread(new ClientHandler(this, sock, nickname , Mark.RED, new SmartStrategy()));
				handler.start();	
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		System.out.println("Give in the portnumber");
		Scanner in = new Scanner(System.in);
		ServerSocket sock = null;
		while (sock == null){
			int portnumber = Integer.parseInt(in.next());
			try{
				sock = new ServerSocket(portnumber);
			}
			catch (IOException e){
				System.out.println("This port:" + portnumber + "is not available, choose another port");
			}
		}

		// And start the server
		Thread s = new Thread (new Server(sock));
		System.out.println("Server starting.");
		s.start();
	}

	/**
	 * Move client to ready map.
	 *
	 * @param client the client
	 * @return true, if successful
	 */
	public boolean moveClientToReady(ClientHandler client) {
		if (joinedMap.containsValue(client)){
			joinedMap.remove(client.getName());
			readyToPlayMap.put(client.getName(), client);
			return (startGameIfPossible(client.getName()));
		}
		return false;

	}

	/**
	 * Starts game if possible.
	 *
	 * @param name the name
	 * @return true, if successful
	 */
	//@requires name != null;
	//@ ensures \result == false || \result == true;
	public boolean startGameIfPossible(String name) {
		boolean gameIsPossible = false;
		if (readyToPlayMap.size() >= 2){
			List<ClientHandler> clients = new ArrayList<ClientHandler>();
			int count = 0;
			for(String n : new HashSet<String>(readyToPlayMap.keySet())) {
				if(count<2) {
					ClientHandler c = readyToPlayMap.get(n);
					clients.add(c);
					inGameMap.put(n, c);
					readyToPlayMap.remove(n);
				}
				count++;
			}
			
			clients.get(0).setMark(Mark.RED);
			clients.get(1).setMark(Mark.YELLOW);
			
			GameController game = new GameController(clients.get(0), clients.get(1));
			games.add(game);
			gameIsPossible = true;
		}
		return gameIsPossible;
	}


	/**
	 * Determines if the player is able to join.
	 *
	 * @param nickname the nickname
	 * @param client the client
	 * @return true, if successful
	 */
	//@ requires nickname != null;
	//@ requires client != null;
	//@ ensures \result == true || \result == false;
	public boolean joinrequest(String nickname, ClientHandler client) {
		if (!joinedMap.containsKey(nickname) && !inGameMap.containsKey(nickname) && !readyToPlayMap.containsKey(nickname)){
			joinedMap.put(nickname, client);
			return true;
		}
		else {
			return false;
		}

	}	
	
	/**
	 * End game. Returns the players to the joinedlist.
	 *
	 * @param game the game
	 */
	//@ requires game != null;
	public void endGame(GameController game){
		if (inGameMap.containsKey(game.players[0].getName()) && inGameMap.containsKey(game.players[1].getName())){
			inGameMap.remove(game.players[0].getName());
			joinedMap.put(game.players[0].getName(), (ClientHandler) game.players[0]);
			inGameMap.remove(game.players[1].getName());
			joinedMap.put(game.players[1].getName(), (ClientHandler) game.players[1]);
			games.remove(game);
		}
	}
	
	
}

package SandC;

import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import game.Board;
import game.Mark;

/**
 * The Class TUI.
 */
public class TUI implements Observer {

	/** The in. */
	private Scanner in = new Scanner(System.in);

	/** The msg. */
	protected String msg;

	/** The nickname. */
	public String nickname;

	/** The client. */
	private Client client;

	/**
	 * Instantiates a new tui.
	 *
	 * @param client
	 *            the client
	 */
	public TUI(Client client) {
		this.client = client;
	}

	/**
	 * Ask name.
	 *
	 * @return the string
	 */
	public String askName() {
		System.out.println("What is your name?");
		Scanner in = new Scanner(System.in);
		String nickname = in.next();
		this.setNickname(nickname);
		return nickname;
	}

	/**
	 * Ask move.
	 * asks the player for the move he/she wants to make and checks if it is a possible move
	 *
	 * @return the string
	 */
	//@pure
	public String askMove() {
		showBoard(client.board);
		String prompt = " " + getNickname() + ", what is your choice? (Divide your coordinates by a space) ";
		System.out.print(prompt);
		Scanner scanner = new Scanner(System.in);
		String msg = null;
		while (msg == null || msg.isEmpty()){
			msg = scanner.hasNextLine() ? scanner.nextLine() : null;
			if (msg == null || msg.isEmpty() || isValidMove(msg)){
				outPutDeniedMove();
			}
		}

		return msg;

	}


	// update method
	@Override
	public void update(Observable boardg, Object boardt) {
		showBoard((Board) boardg);
	}

	/**
	 * Show board.
	 * Representation of the board
	 *
	 * @param board
	 *            the board
	 */
	//@pure
	public void showBoard(Board board) {
		System.out.println("\ncurrent game situation: \n\n" + "y = 0" + "\n" + board.toString(0, board) + "\n" + "\n");
		System.out.println("y = 1" + "\n" + board.toString(1, board) + "\n" + "\n");
		System.out.println("y = 2" + "\n" + board.toString(2, board) + "\n" + "\n");
		System.out.println("y = 3" + "\n" + board.toString(3, board) + "\n" + "\n");
	}

	/**
	 * Checks if is valid move.
	 *
	 * @param msg
	 *            the msg
	 * @return the string
	 */
	//@ requires msg != null;
	//@ ensures \result == true || \result == false;
	public boolean isValidMove(String msg) {
		String[] ar = msg.split(" ");
		if (ar.length == 2) {
			int choicex = Integer.parseInt(ar[0]);
			int choicez = Integer.parseInt(ar[1]);
			if (choicex >= 0 && choicex < 4 && choicez >= 0 && choicez < 4) {
				return true;
			} else {
				return false;

			}
		} else {
			return false;
		}
	}

	/**
	 * Out put accept request.
	 */
	//@pure
	public void outPutAcceptRequest() {
		System.out.println("You are connected to the server.");
	}

	/**
	 * Out put denied request.
	 */
	//@pure
	public void outPutDeniedRequest() {
		System.out.println("Server denied request, pick another nickname.");
	}

	/**
	 * Out put start game.
	 */
	//@pure
	public void outPutStartGame() {
		System.out.println("There is a game started");
	}

	/**
	 * Out put denied move.
	 */
	//@pure
	public void outPutDeniedMove() {
		System.out.println("Your move cannot be made, choose another move");
	}

	/**
	 * Out put you win.
	 */
	//@pure
	public void outPutYouWin() {
		System.out.println("You won!");
	}

	/**
	 * Out put you lost.
	 */
	//@pure
	public void outPutYouLost() {
		System.out.println("Unfortunately, you lost!");
	}

	/**
	 * Out put invalid command.
	 */
	//@pure
	public void outPutInvalidCommand() {
		System.out.println("Invalid command");
	}

	/**
	 * Out put your connection lost.
	 */
	//@pure
	public void outPutYourConnectionLost() {
		System.out.println("You lost connection");
	}

	/**
	 * Out put other connection lost.
	 */
	//@pure
	public void outPutOtherConnectionLost() {
		System.out.println("Your opponent lost connection");
	}

	/**
	 * Wait opponent.
	 */
	//@pure
	public void waitOpponent() {
		System.out.println("The server is waiting for opponent");
	}
	//@pure
	public void outPutInvalidInput(){
		System.out.println("Invalid input");
	}

	/**
	 * Aks the player if he/she is ready to play game.
	 *
	 * @return true, if successful
	 */
	//@pure
	public boolean readyToPlayGame() {
		System.out.println("Do you want to play a game? (yes/no)");
		Scanner in = new Scanner(System.in);
		String answer = in.next();
		if (answer.equals("yes")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Gets the nickname.
	 *
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * Sets the nickname.
	 *
	 * @param nickname
	 *            the new nickname
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * Prints a question which can be answered by yes (true) or no (false).
	 * After prompting the question on standard out, this method reads a String
	 * from standard in and compares it to the parameters for yes and no. If the
	 * user inputs a different value, the prompt is repeated and the method
	 * reads input again.
	 * 
	 * @param prompt
	 *            the question to print
	 * @param yes
	 *            the String corresponding to a yes answer
	 * @param no
	 *            the String corresponding to a no answer
	 * @return true is the yes answer is typed, false if the no answer is typed
	 */
	protected boolean readBoolean(String prompt, String yes, String no) {
		String answer;
		System.out.print(prompt);
		Scanner readYesNo = new Scanner(System.in);
		answer = readYesNo.nextLine();
		if (!answer.equals(yes) && !answer.equals(no)) {
			System.out.println("Invalid input, type yes or no");
			System.out.print(prompt);
			Scanner readYesNo2 = new Scanner(System.in);
			answer = readYesNo2.next();
		}
		else if (answer.equals(no)){
			try {
				Thread.sleep((long) 1500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			readBoolean("\n> Play another time? (yes/no)?", "yes", "no");
		}
		return (answer.equals(yes));

	}

	/**
	 * Asks if the player is a computer player or human player.
	 *
	 * @return true, if successful
	 */
	public boolean askComputerPlayerOrHumanPlayer() {
		System.out.println("Are you a humanplayer? (yes/no)");
		Scanner in = new Scanner(System.in);
		String ar = in.next();
		return ar.equals("yes");
	}
	
	//@ ensures \result >= -1;
	public int setTime() {
		System.out.println("What is the thinkingtime of the Computer in miliseconds?");
		Scanner scanner = new Scanner(System.in);
		if (in.hasNext()){
			int time2 = Integer.parseInt(in.next());
			return time2;
		}
		else{
			return -1;
		}
	}
	
	public void outPutDraw(){
		System.out.println("It's draw");
	}
	
	public void outPutConnectionServerLost(){
		System.out.println("Server connection is lost");
	}
}

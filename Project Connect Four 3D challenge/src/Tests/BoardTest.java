package Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import game.Board;
import game.Mark;

public class BoardTest {
	private Board board;
	private Board board2;
	private Board board3; //
	private Board board4; // used for the 2DZ and 2DY diagonals
	private Board board5; // used for the 2DY and 2DZ diagonals
	private Board board6; // used for the 2DX diagonals
	private Board board7; // used for the 3D diagonals
	private Board board8; // used for the 3D diagonals
	private Board boardcopy; // used for deepCopy()
	private Board board9; // used for only one 3D diagonal
	
	@Before
	public void setUp() throws Exception {
		Board board = new Board();
		this.board= board;
		
		Board boardcopy = board.deepCopy();
		this.boardcopy = boardcopy;
		
		Board board2 = new Board();
		this.board2=board2;
		for (int x = 0; x < 4; x++){
			for (int z = 0; z< 4; z++){
				for (int y = 0; y < 4; y++){
					board2.setField(x, z, y, Mark.RED);
				}
			}
		}
		
		Board board3 = new Board();
		this.board3 = board3;
		board3.setField(0, 0, 0, Mark.RED);
		board3.setField(0, 0, 1, Mark.RED);
		board3.setField(0, 0, 2, Mark.RED);
		board3.setField(0, 0, 3, Mark.RED);
		
		
		
		Board board4 = new Board();
		this.board4 = board4;
		
		board4.setField(3, 0, 0, Mark.RED);
		board4.setField(2, 1, 0, Mark.RED);
		board4.setField(1, 2, 0, Mark.RED);
		board4.setField(0, 3, 0, Mark.RED);
		
		board4.setField(0, 0, 0, Mark.YELLOW);
		board4.setField(1, 0, 1, Mark.YELLOW);
		board4.setField(2, 0, 2, Mark.YELLOW);
		board4.setField(3, 0, 3, Mark.YELLOW);
		
		
		
		Board board5 = new Board();
		this.board5 = board5;
		board5.setField(0, 0, 0, Mark.YELLOW);
		board5.setField(1, 1, 0, Mark.YELLOW);
		board5.setField(2, 2, 0, Mark.YELLOW);
		board5.setField(3, 3, 0, Mark.YELLOW);
		

		board5.setField(3, 0, 0, Mark.RED);
		board5.setField(2, 0, 1, Mark.RED);
		board5.setField(1, 0, 2, Mark.RED);
		board5.setField(0, 0, 3, Mark.RED);
		
		
		
		Board board6 = new Board();
		this.board6 = board6;
		board6.setField(0, 0, 0, Mark.RED);
		board6.setField(0, 1, 1, Mark.RED);
		board6.setField(0, 2, 2, Mark.RED);
		board6.setField(0, 3, 3, Mark.RED);
		
		Board board7 = new Board();
		this.board7 = board7;
		board7.setField(0, 0, 0, Mark.RED);
		board7.setField(1, 1, 1, Mark.RED);
		board7.setField(2, 2, 2, Mark.RED);
		board7.setField(3, 3, 3, Mark.RED);
		
		board7.setField(0, 0, 3, Mark.YELLOW);
		board7.setField(1, 1, 2, Mark.YELLOW);
		board7.setField(2, 2, 1, Mark.YELLOW);
		board7.setField(3, 3, 0, Mark.YELLOW);
		
		Board board8 = new Board();
		this.board8 = board8;
		board8.setField(3, 0, 0, Mark.RED);
		board8.setField(2, 1, 1, Mark.RED);
		board8.setField(1, 2, 2, Mark.RED);
		board8.setField(0, 3, 3, Mark.RED);
		
		board8.setField(3, 0, 3, Mark.YELLOW);
		board8.setField(2, 1, 2, Mark.YELLOW);
		board8.setField(1, 2, 1, Mark.YELLOW);
		board8.setField(0, 3, 0, Mark.YELLOW);
		
		Board board9= new Board();
		board9.setField(0, 0, 0, Mark.RED);
		board9.setField(1, 1, 1, Mark.RED);
		board9.setField(2, 2, 2, Mark.RED);
		board9.setField(3, 3, 3, Mark.RED);
		this.board9 = board9;
	}
	
	@Test
	public void getField(){
		assertEquals("getField", Mark.EMPTY, board.getField(0, 1, 3));
		assertEquals("getField", Mark.EMPTY, board.getField(3, 2, 1));
	}
	
	@Test
	public void clearBoardTest() {
		board.clearBoard();
		assertEquals("clearboard", Mark.EMPTY, board.getField(0, 0, 0));
		assertEquals("clearboard", Mark.EMPTY, board.getField(2, 1, 1));
		assertEquals("clearboard", Mark.EMPTY, board.getField(3, 3, 3));
		assertEquals("clearboard", Mark.EMPTY, board.getField(1, 1, 1));
		assertEquals("clearboard", Mark.EMPTY, board.getField(1, 2, 3));
	}
	
	@Test
	public void setFieldTest(){
		board.setField(0, 0, 0, Mark.RED);
		board.setField(3, 2, 3, Mark.YELLOW);
		board.setField(2, 1, 3, Mark.RED);
		board.setField(1, 3, 2, Mark.YELLOW);
		assertEquals("setField", Mark.RED, board.getField(0,0,0));
		assertEquals("setField", Mark.YELLOW, board.getField(3,2,3));
		assertEquals("setField", Mark.RED, board.getField(2,1,3));
		assertEquals("setField", Mark.YELLOW, board.getField(1, 3, 2));
	}
	
	@Test
	public void isFieldTest(){
		assertTrue(board.isField(1,2,3));
		assertFalse(board.isField(5, 3, 1));
		assertTrue(board.isField(3, 2, 2));
		assertFalse(board.isField(9, 4, 1));
	}
	
	@Test
	public void isEmptyFieldTest(){
		board.setField(2, 1, 3, Mark.RED);
		board.setField(1, 3, 3, Mark.RED);
		assertTrue(board.isEmptyField(3, 1, 2));
		assertTrue(board.isEmptyField(2, 2, 2));
		assertFalse(board.isEmptyField(1, 3, 3));
		assertFalse(board.isEmptyField(2, 1, 3));
	}
	
	@Test
	public void isFullTest(){
		for (int x = 0; x < 4; x++){
			for (int z = 0; z< 4; z++){
				for (int y = 0; y < 4; y++){
					board2.setField(x, z, y, Mark.RED);
				}
			}
		}
		
		assertFalse(board.isFull());
		assertTrue(board2.isFull());
		
	}
	
	@Test 
	public void calculateYTest(){
		assertEquals("calculateY on full board", -1, board2.calculateY(3, 0));
		assertEquals("calculateY", 0, board.calculateY(2, 1));
	}
	
	@Test
	public void gameOverTest(){
		board3.setField(0, 0, 0, Mark.RED);
		board3.setField(0, 0, 1, Mark.RED);
		board3.setField(0, 0, 2, Mark.RED);
		board3.setField(0, 0, 3, Mark.RED);
		
		assertTrue(board2.gameOver());
		assertTrue(board3.gameOver());
		assertFalse(board.gameOver());
	}
	
	@Test
	public void hasRowDifferentYTest(){
		assertTrue(board3.hasRowDifferentY(Mark.RED));
		assertFalse(board3.hasRowDifferentY(Mark.YELLOW));
		assertFalse(board.hasRowDifferentY(Mark.YELLOW));
	}
	
	@Test
	public void hasRowDifferentZTest(){
		board.setField(0, 0, 0, Mark.RED);
		board.setField(0, 1, 0, Mark.RED);
		board.setField(0, 2, 0, Mark.RED);
		board.setField(0, 3, 0, Mark.RED);
		assertTrue(board.hasRowDifferentZ(Mark.RED));
		assertFalse(board3.hasRowDifferentZ(Mark.YELLOW));
		assertFalse(board2.hasRowDifferentZ(Mark.YELLOW));
	}
	
	@Test
	public void hasRowDifferentXTest(){
		assertTrue(board2.hasRowDifferentX(Mark.RED));
		assertFalse(board.hasRowDifferentX(Mark.YELLOW));
		assertFalse(board3.hasRowDifferentX(Mark.RED));
	}
	
	@Test
	public void hasDiagonal2DZTest(){
		assertTrue(board2.hasDiagonal2DZ(Mark.RED));
		assertTrue(board4.hasDiagonal2DZ(Mark.YELLOW));
		assertFalse(board.hasDiagonal2DZ(Mark.RED));		
		assertTrue(board5.hasDiagonal2DZ(Mark.RED));
		assertFalse(board2.hasDiagonal2DZ(Mark.YELLOW));
	}
	
	@Test
	public void hasDiagonal2DYTest(){
		assertFalse(board.hasDiagonal2DY(Mark.RED));
		assertFalse(board2.hasDiagonal2DY(Mark.YELLOW));
		
		assertTrue(board5.hasDiagonal2DY(Mark.YELLOW));
		assertFalse(board2.hasDiagonal2DY(Mark.YELLOW));
		assertFalse(board3.hasDiagonal2DY(Mark.RED));
		
		assertTrue(board4.hasDiagonal2DY(Mark.RED));
	}
	
	@Test
	public void hasDiagonal2DXTest(){
		
		assertTrue(board6.hasDiagonal2DX(Mark.RED));
		assertFalse(board2.hasDiagonal2DX(Mark.YELLOW));
		assertFalse(board3.hasDiagonal2DX(Mark.RED));
		assertTrue(board2.hasDiagonal2DX(Mark.RED));
		
		board3.setField(0, 3, 0, Mark.YELLOW);
		board3.setField(0, 2, 1, Mark.YELLOW);
		board3.setField(0, 1, 2, Mark.YELLOW);
		board3.setField(0, 0, 3, Mark.YELLOW);
		
		assertTrue(board3.hasDiagonal2DX(Mark.YELLOW));
		assertFalse(board2.hasDiagonal2DX(Mark.YELLOW));
		assertTrue(board2.hasDiagonal2DX(Mark.RED));
		assertFalse(board.hasDiagonal2DX(Mark.RED));
	}
	
	@Test
	public void hasDiagonal3DTest(){

		assertTrue(board8.hasDiagonal3D(Mark.YELLOW));
		assertTrue(board7.hasDiagonal3D(Mark.RED));
		assertTrue(board7.hasDiagonal3D(Mark.YELLOW));
		assertTrue(board8.hasDiagonal3D(Mark.RED));
		assertFalse(board.hasDiagonal3D(Mark.RED));
		assertFalse(board2.hasDiagonal3D(Mark.YELLOW));
		assertFalse(board3.hasDiagonal3D(Mark.YELLOW));
		
	}
	
	@Test 
	public void isWinnerTest(){
		assertTrue(board8.isWinner(Mark.RED));
		assertTrue(board3.isWinner(Mark.RED));
		assertTrue(board6.isWinner(Mark.RED));
		assertTrue(board5.isWinner(Mark.RED));
		assertTrue(board4.isWinner(Mark.YELLOW));
		assertTrue(board2.isWinner(Mark.RED));
		assertTrue(board5.isWinner(Mark.YELLOW));
		assertTrue(board4.isWinner(Mark.YELLOW));
		assertFalse(board.isWinner(Mark.YELLOW));
		assertFalse(board.isWinner(Mark.YELLOW));
		assertFalse(board6.isWinner(Mark.YELLOW));
		assertFalse(board3.isWinner(Mark.YELLOW));
		assertTrue(board9.isWinner(Mark.RED));
	}
	
	@Test
	public void hasWinnerTest(){
		assertTrue(board8.hasWinner());
		assertTrue(board3.hasWinner());
		assertTrue(board5.hasWinner());
		assertTrue(board6.hasWinner());
		assertTrue(board4.hasWinner());
	}
	
	@Test
	public void deepCopyTest(){
		for (int x = 0; x <4; x++){
			for (int z = 0; z < 4; z++){
				for (int y = 0; y < 4; y++){
					assertEquals("deepcopy Empty Board", Mark.EMPTY, boardcopy.getField(x,z,y));
				}
			}
		}
		
	}
}

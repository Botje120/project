package game;

import java.util.Observable;

/**
 * The Class Board.
 */
public class Board extends Observable {

	/** The board. */
	private Mark[][][] board;

	/** The Constant NUMBERING. */
	private static final String[] NUMBERING = { " 0,0 | 0,1 | 0,2 | 0,3 ", "---------------", " 1,0 | 1,1 | 1,2 | 1,3 ",
			"---------------", " 2,0 | 2,1 | 2,2 | 2,3", "---------------", " 3,0 | 3,1 | 3,2 | 3,3 " };

	/**
	 * Creates an Empty board.
	 */
	public Board() {
		board = new Mark[4][4][4];
		clearBoard();
	}

	/**
	 * Mark the fields over the board with Mark.EMPTY
	 */
	// @ ensures (\forall integer x, integer y integer z; 0 <= x & x < 4 & 0<= z
	// & z < 4 & 0 <= y & y < 4; this.getField(x,z,y) == Mark.EMPTY);
	public void clearBoard() {
		for (int x = 0; x < 4; x++) {
			for (int z = 0; z < 4; z++) {
				for (int y = 0; y < 4; y++) {
					setField(x, z, y, Mark.EMPTY);
				}
			}
		}
	}

	/**
	 * Deep copy.
	 *
	 * @return a copy of the board created in the constructor, thus the board
	 *         will be EMPTY
	 */
	// @ ensures \result != this;
	public Board deepCopy() {
		Board copy = new Board();
		return copy;
	}

	/**
	 * Set a mark to a specific point.
	 *
	 * @param x
	 *            the index of the x-as of the point you want to set a mark
	 * @param z
	 *            the index of the z-as of the point you want to set a mark
	 * @param y
	 *            the index of the y-as of the point you want to set a mark
	 * @param m
	 *            the mark you want to set
	 */
	// @ requires this.isField(x,z,y);
	// @ ensures this.getField(x,z,y) == m;
	public void setField(int x, int z, int y, Mark m) {
		board[x][z][y] = m;
		this.setChanged();
		this.notifyObservers();
	}

	/**
	 * Checks if the x, y, z index together is a field on the board.
	 *
	 * @param x
	 *            the index of the x-as you want to check if it is a field on
	 *            the board
	 * @param z
	 *            the index of the z-as you want to check if it is a field on
	 *            the board
	 * @param y
	 *            the index of the y-as you want to check if it is a field on
	 *            the board
	 * @return true if the indexes of x, y, z together form a valid field on the
	 *         board
	 */
	// @ ensures \result == ((0 <= x) && (x <= 4) && (0 <= z) && (z <= 4) && (0
	// <= y) && (y<= 4));
	/* @pure */
	public boolean isField(int x, int z, int y) {
		return ((0 <= x) && (x <= 4) && (0 <= z) && (z <= 4) && (0 <= y) && (y <= 4));
	}

	/**
	 * Returns the content of the field (x,y,z).
	 *
	 * @param x
	 *            the index of the x-as of the point you want to return the mark
	 *            of.
	 * @param z
	 *            the index of the z-as of the point you want to return the mark
	 *            of.
	 * @param y
	 *            the index of the y-as of the point you want to return the mark
	 *            of.
	 * @return the mark on the field
	 */
	// @ requires this.isField(x,y,z);
	// @ ensures \result == Mark.EMPTY || \result == Mark.RED || \result ==
	// Mark.YELLOW;
	/* @pure */
	public Mark getField(int x, int z, int y) {
		return board[x][z][y];
	}

	/**
	 * Returns true if the field (x,z,y) .
	 *
	 * @param x
	 *            the index of the x-as of the point you want to check if the
	 *            field is EMPTY.
	 * @param z
	 *            the index of the z-as of the point you want to check if the
	 *            field is EMPTY.
	 * @param y
	 *            the index of the y-as of the point you want to check if the
	 *            field is EMPTY.
	 * @return true if the field is empty.
	 */
	// @ requires this.isField(x,y,z);
	// @ ensures \result == (this.getField(x,y,z) == Mark.EMPTY);
	/* @pure */
	public boolean isEmptyField(int x, int z, int y) {
		return getField(x, z, y) == Mark.EMPTY;
	}

	/**
	 * Tests if the whole board is full.
	 * 
	 * @return true if all fields are occupied.
	 */
	// @ ensures \result == (\forall integer x; integer y; integer z; 0 <= x & x
	// < 4 & 0<= z & z < 4 & 0 <= y & y < 4; this.getField(x,z,y) !=
	// Mark.EMPTY);
	/* @pure */
	public boolean isFull() {
		boolean full = true;
		for (int x = 0; x < 4; x++) {
			for (int z = 0; z < 4; z++) {
				for (int y = 0; y < 4; y++) {
					if (isEmptyField(x, z, y)) {
						full = false;
						break;
					}
				}
			}
		}
		return full;
	}

	/**
	 * Calculates at which y the mark should be placed if you want put the mark
	 * on a place with a specific x and z. The z is not selectable, because it
	 * drops to the lowest point if you throw in a mark.
	 * 
	 * @param x
	 *            the index of the x-as of the point
	 * @param z
	 *            the index of the z-as of the point
	 * @return the y integer of the EmptyField(x,z) with the lowest y.
	 */
	// @ requires (0 <= x) && (x < 4) && (0 <= z) && (z < 4);
	// @ ensures (\result >= -1) && (\result < 4);
	/* @pure */
	public int calculateY(int x, int z) {
		for (int y = 0; y < 4; y++) {
			if (isEmptyField(x, z, y)) {
				return y;
			}
		}
		return -1;
	}

	/**
	 * Returns true if the game is over. The game is over when there is a winner
	 * or the whole board is full.
	 *
	 * @return true if the game is over
	 */
	// @ ensures \result == this.isFull() || this.hasWinner();
	/* @pure */
	public boolean gameOver() {
		return (isFull() || hasWinner());
	}

	/**
	 * Checks whether there is a row which is full on the board in the
	 * y-direction.
	 * 
	 * @param m
	 *            the mark you want to check on
	 * @return true if there is a row in the y-direction controlled by m.
	 */
	/* @ pure */
	public boolean hasRowDifferentY(Mark m) {
		boolean hasRowDifferentY = false;
		for (int x = 0; x < 4; x++) {
			for (int z = 0; z < 4; z++) {
				int c = 0;
				for (int y = 0; y < 4; y++) {
					if (getField(x, z, y) == m) {
						c++;
					}
				}
				if (c == 4) {
					hasRowDifferentY = true;
					break;
				}
			}

		}

		return hasRowDifferentY;
	}

	/**
	 * Checks whether there is a row which is full on the board in the
	 * z-direction.
	 * 
	 * @param m
	 *            the mark youse want to check on
	 * @return true if there is a row in the z-direction controlled by m.
	 */
	/* @ pure */
	public boolean hasRowDifferentZ(Mark m) {
		boolean hasRowDifferentZ = false;
		for (int x = 0; x < 4; x++) {
			for (int y = 0; y < 4; y++) {
				int c = 0;
				for (int z = 0; z < 4; z++) {
					if (getField(x, z, y) == m) {
						c++;
					}
				}
				if (c == 4) {
					hasRowDifferentZ = true;
					break;
				}
			}

		}

		return hasRowDifferentZ;
	}

	/**
	 * Checks whether there is a row which is full on the board in the
	 * x-direction and only contains the mark m.
	 * 
	 * @param m
	 *            the mark you want to check on
	 * @return true if there is a row in the x-direction controlled by m.
	 */
	/* @ pure */
	public boolean hasRowDifferentX(Mark m) {
		boolean hasRowDifferentX = false;
		for (int y = 0; y < 4; y++) {
			for (int z = 0; z < 4; z++) {
				int c = 0;
				for (int x = 0; x < 4; x++) {
					if (getField(x, z, y) == m) {
						c++;
					}
				}
				if (c == 4) {
					hasRowDifferentX = true;
					break;
				}
			}

		}

		return hasRowDifferentX;
	}

	/**
	 * Checks whether there is a 2D-diagonal in the z-direction which is full
	 * and only contains the mark m.
	 * 
	 * @param m
	 *            the mark of interest
	 * @return true if there is a 2D-diagonal in the z-direction controlled by
	 *         m.
	 */
	/* @ pure */
	public boolean hasDiagonal2DZ(Mark m) {
		boolean hasDiagonal2DZ1 = false;
		boolean hasDiagonal2DZ2 = false;
		for (int z = 0; z < 4; z++) {
			int c = 0;
			while (getField(c, z, c) == m) {
				c++;
				if (c == 4) {
					hasDiagonal2DZ1 = true;
					break;
				}
			}

		}

		for (int z = 0; z < 4; z++) {
			int c = 3;
			while (getField(c, z, (3 - c)) == m) {
				c--;
				if (c == -1) {
					hasDiagonal2DZ2 = true;
					break;
				}

			}

		}

		return hasDiagonal2DZ1 || hasDiagonal2DZ2;
	}

	/**
	 * Checks whether there is a 2D-diagonal in the y-direction which is full
	 * and only contains the mark m.
	 * 
	 * @param m
	 *            the mark of interest
	 * @return true if there is a 2D-diagonal in the y-direction controlled by
	 *         m.
	 */
	/* @ pure */
	public boolean hasDiagonal2DY(Mark m) {
		boolean hasDiagonal2DY1 = false;
		boolean hasDiagonal2DY2 = false;
		for (int y = 0; y < 4; y++) {
			int c = 0;
			while (getField(c, c, y) == m) {
				c++;
				if (c == 4) {
					hasDiagonal2DY1 = true;
					break;
				}
			}

		}

		for (int y = 0; y < 4; y++) {
			int c = 3;
			while (getField(c, (3 - c), y) == m) {
				c--;
				if (c == -1) {
					hasDiagonal2DY2 = true;
					break;
				}
			}

		}

		return (hasDiagonal2DY1 || hasDiagonal2DY2);
	}

	/**
	 * Checks whether there is a 2D-diagonal in the x-direction which is full
	 * and only contains the mark m.
	 * 
	 * @param m
	 *            the mark of interest
	 * @return true if there is a 2D-diagonal in the x-direction controlled by
	 *         m.
	 */
	/* @ pure */
	public boolean hasDiagonal2DX(Mark m) {
		boolean hasDiagonal2DX1 = false;
		boolean hasDiagonal2DX2 = false;
		for (int x = 0; x < 4; x++) {
			int c = 0;
			while (getField(x, c, c) == m) {
				c++;
				if (c == 4) {
					hasDiagonal2DX1 = true;
					break;
				}
			}

		}

		for (int x = 0; x < 4; x++) {
			int c = 3;
			while (getField(x, c, (3 - c)) == m) {
				c--;
				if (c == -1) {
					hasDiagonal2DX2 = true;
					break;
				}
			}

		}

		return hasDiagonal2DX1 || hasDiagonal2DX2;
	}

	/**
	 * Checks whether there is a 3D-diagonal which is full and only contains
	 * Mark m.
	 * 
	 * @param m
	 *            the mark of interest
	 * @return true if there is a 3D-diagonal controlled by m.
	 */
	/* @ pure */
	public boolean hasDiagonal3D(Mark m) {
		boolean hasDiagonal3D = false;
		int d = 0;
		for (int c = 0; c < 4; c++) {
			if (getField(c, c, c) == m) {
				d++;
			}
			if (d == 4) {
				hasDiagonal3D = true;
				break;
			}
		}
		int e = 0;
		for (int c = 3; c >= 0; c--) {
			if (getField(c, (3 - c), (3 - c)) == m) {
				e++;
			}
			if (e == 4) {
				hasDiagonal3D = true;
				break;
			}
		}

		int f = 0;
		for (int c = 0; c < 4; c++) {
			if (getField(c, c, (3 - c)) == m) {
				f++;
			}
			if (f == 4) {
				hasDiagonal3D = true;
				break;
			}
		}

		int g = 0;
		for (int c = 0; c < 4; c++) {
			if (getField(c, (3 - c), c) == m) {
				g++;
			}
			if (g == 4) {
				hasDiagonal3D = true;
				break;
			}
		}

		return hasDiagonal3D;
	}

	/**
	 * Checks if the mark m has won. A mark wins if it controls at least one
	 * row, 2D-diagonal or 3D-diagonal.
	 *
	 * @param m
	 *            the mark of interest
	 * @return true if the mark has won.
	 */
	// @requires m == Mark.RED || m == Mark.YELLOW;
	// @ ensures \result == (hasRowDifferentZ(m) || hasRowDifferentY(m) ||
	// hasRowDifferentX(m) || hasDiagonal2DY (m) || hasDiagonal2DZ (m) ||
	// hasDiagonal2DX (m) || hasDiagonal3D (m));
	/* @ pure */
	public boolean isWinner(Mark m) {
		assert m == Mark.RED || m == Mark.YELLOW;
		return (hasRowDifferentZ(m) || hasRowDifferentY(m) || hasRowDifferentX(m) || hasDiagonal2DZ(m)
				|| hasDiagonal2DY(m) || hasDiagonal2DX(m) || hasDiagonal3D(m));
	}

	/**
	 * Returns true if the game has a winner. This is the case
	 * isWinner(Mark.RED) is true or/and isWinner(Mark.YELLOW) is true.
	 *
	 * @return true if the board has a winner.
	 */
	// @ ensures \result == isWinner(Mark.RED) || \result ==
	// isWinner(Mark.YELLOW);
	/* @pure */
	public boolean hasWinner() {
		return (isWinner(Mark.RED) || isWinner(Mark.YELLOW));
	}

	/**
	 * Returns a String representation of this board. Shows four layers with a
	 * different z. It also shows you what coordinates match which place.
	 *
	 * @param y
	 *            is the z of the
	 * @param board
	 *            the board
	 * @return the game situation as String
	 */
	public String toString(int y, Board board) {
		assert y >= 0 && y < 4;
		String s = "";
		for (int x = 0; x < 4; x++) {
			String row = "";
			for (int z = 0; z < 4; z++) {
				row = row + " " + board.getField(x, z, y).toString() + " ";
				if (z < 3) {
					row = row + "|";
				}
			}
			s = s + row + "      " + NUMBERING[x * 2];
			if (x < 3) {
				s = s + "\n" + NUMBERING[1] + NUMBERING[x * 2 + 1] + "\n";
			}
		}
		return s;
	}

}

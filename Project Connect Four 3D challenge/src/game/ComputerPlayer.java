package game;

import java.util.ArrayList;
import java.util.Observable;


/**
 * The Class ComputerPlayer.
 */
public class ComputerPlayer extends Player {

/** The strat. */
private Strategy  strat;
	
	/** Creates a ComputerPlayer object.
	 * 
	 * @param mark			the mark of the ComputerPlayer
	 * @param strategy		the strategy which the ComputerPlayer will play with
	 */
	//@pure
	public ComputerPlayer (Mark mark, Strategy strategy){
		super((strategy.getName() + "-" + mark), mark);
		strat = strategy;
	}
	
	/**
	 * Creates a ComputerPlayer object.
	 * @param mark			the mark of the ComputerPlayer
	 */
	//@pure
	public ComputerPlayer (Mark mark){
		this(mark, new SmartStrategy());
	}
	
	/*@
	requires board != null & !board.isFull();
	ensures board.isField(\result.get(0), \result.get(1), board.calculateY(\result.get(0), \result.get(1))) & board.isEmptyField(\result.get(0), \result.get(1), board.calculateY(\result.get(0), \result.get(1)));
	 */
	/**
	 * Determines the field for the next move. 
	 *
	 * @param board the board
	 * @return the player's choice
	 */
	@Override
	public ArrayList<Integer> determineMove(Board board) {
		return strat.determineMove(board, getMark());
	}

}

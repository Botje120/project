package game;

/**
 * The Class ConnectFour.
 */
public class ConnectFour {
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		if (args.length == Game.NUMBER_PLAYERS){
			Game game = new Game((createPlayer(args[0], Mark.RED)), (createPlayer(args[1], Mark.YELLOW)));
			game.start();
		}
		else {
			System.out.println("You have to play this game with two players.");
		}
		
	}
	
	/**
	 * Creates the player.
	 *
	 * @param name the name
	 * @param m the m
	 * @return the player
	 */
	/*@
	 	requires name != null;
	 	requires m == Mark.RED || m == Mark.YELLOW;
	 */
	public static Player createPlayer(String name, Mark m){
		if (name.equals("-N")){
			return new ComputerPlayer(m);
		}
		else if (name.equals("-S")){
			return new ComputerPlayer(m, new SmartStrategy());
		}
		else {
			return new HumanPlayer (name, m);
		}
	}
}

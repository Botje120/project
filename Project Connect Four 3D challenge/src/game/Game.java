package game;

import java.util.Scanner;

/**
 * The Class Game.
 */
public class Game {
	
	/** The Constant NUMBER_PLAYERS. */
	public static final int NUMBER_PLAYERS = 2;

	/*@
	       private invariant board != null;
	 */
	/**
	 * The board.
	 */
	private Board board;

	/*@
	       private invariant players.length == NUMBER_PLAYERS;
	       private invariant (\forall int i; 0 <= i && i < NUMBER_PLAYERS; players[i] != null); 
	 */
	/**
	 * The 2 players of the game.
	 */
	private Player[] players;

	/*@
	       private invariant 0 <= current  && current < NUMBER_PLAYERS;
	 */
	/**
	 * Index of the current player.
	 */
	private int current;


	/*@
	      requires s0 != null;
	      requires s1 != null;
	 */
	/**
	 * Creates a new Game object.
	 * 
	 * @param s0
	 *            the first player
	 * @param s1
	 *            the second player
	 */
	public Game(Player s0, Player s1) {
		setBoard(new Board());
		setPlayers(new Player[NUMBER_PLAYERS]);
		getPlayers()[0] = s0;
		getPlayers()[1] = s1;
		current = 0;
	}

	/**
	 * Starts the ConnectFour game.
	 * Asks after each ended game if the user want to continue. Continues until
	 * the user does not want to play anymore.
	 */
	public void start() {
		boolean continu = true;
		while (continu) {
			reset();
			play();
			continu = readBoolean("\n> Play another time? (yes/no)?", "yes", "no");
		}
	}
	
	/**
	 * Prints a question which can be answered by yes (true) or no (false).
	 * After prompting the question on standard out, this method reads a String
	 * from standard in and compares it to the parameters for yes and no. If the
	 * user inputs a different value, the prompt is repeated and the method reads
	 * input again.
	 * 
	 * @param prompt the question to print
	 * @param yes
	 *            the String corresponding to a yes answer
	 * @param no
	 *            the String corresponding to a no answer
	 * @return true is the yes answer is typed, false if the no answer is typed
	 */
	private boolean readBoolean (String prompt, String yes, String no){
		String answer;
		System.out.print(prompt);
		Scanner readYesNo = new Scanner (System.in); 
		answer = readYesNo.next();
		if (!answer.equals(yes) && !answer.equals(no)){
			System.out.println("Invalid input, type yes or no");
			System.out.print(prompt);
			Scanner readYesNo2 = new Scanner (System.in); 
			answer = readYesNo2.next();
		}
		return (answer.equals(yes));
		

	}
	
	/**
	 * Resets the game.
	 * The board is emptied and player[0] becomes the current player.
	 */
	private void reset() {
		current = 0;
		getBoard().clearBoard();
	}

	/**
	 * Plays Connect Four game.
	 * First the (still empty) board is shown. Then the game is played until it
	 * is over. Players can make a move one after the other. After each move,
	 * the changed game situation is printed.
	 */
	private void play() {
		update();

		while (!getBoard().hasWinner() && !getBoard().isFull()){
			getPlayers()[current].makeMove(getBoard());
			current++;
			if (current == NUMBER_PLAYERS){
				current = 0;
			}
			update();
		}

		printResult();
	}

	/**
	 * Prints the current game situation.
	 */
	private void update() {
		System.out.println("\ncurrent game situation: \n\n" + "y = 0" + "\n" + getBoard().toString(0, getBoard())
		+ "\n" + "\n");
		System.out.println("y = 1"+ "\n" + getBoard().toString(1, getBoard()) + "\n" + "\n");
		
		System.out.println("y = 2" + "\n" + getBoard().toString(2, getBoard()) + "\n" + "\n");
		System.out.println("y = 3" + "\n" + getBoard().toString(3, getBoard()) + "\n" + "\n");
	}

	/*@
	       requires this.board.gameOver();
	 */

	/**
	 * Prints the result of the last game.
	 */
	private void printResult() {
		if (getBoard().hasWinner()) {
			Player winner = getBoard().isWinner(getPlayers()[0].getMark()) ? getPlayers()[0]
					: getPlayers()[1];
			System.out.println("Speler " + winner.getName() + " ("
					+ winner.getMark().toString() + ") has won!");
		} else {
			System.out.println("Draw. There is no winner!");
		}
	}

	/**
	 * Gets the board.
	 *
	 * @return the board
	 */
	public Board getBoard() {
		return board;
	}

	/**
	 * Sets the board.
	 *
	 * @param board the new board
	 */
	public void setBoard(Board board) {
		this.board = board;
	}

	/**
	 * Gets the players.
	 *
	 * @return the players
	 */
	public Player[] getPlayers() {
		return players;
	}

	/**
	 * Sets the players.
	 *
	 * @param players the new players
	 */
	public void setPlayers(Player[] players) {
		this.players = players;
	}
}

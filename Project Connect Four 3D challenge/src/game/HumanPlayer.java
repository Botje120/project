package game;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Scanner;

/**
 * The Class HumanPlayer.
 */
public class HumanPlayer extends Player {


	/*@
    requires name != null;
    requires mark == Mark.RED || mark == Mark.YELLOW;
    ensures this.getName() == name;
    ensures this.getMark() == mark;
	 */
	/**
	 * Creates a new human player object.
	 *
	 * @param name the name
	 * @param mark the mark
	 */
	public HumanPlayer(String name, Mark mark) {
		super(name, mark);
	}

	/*@
	 	requires !board.isFull();
    	requires board != null;
    	ensures board.isField(\result.get(0), \result.get(1), board.calculateY(\result.get(0), \result.get(1))) && board.isEmptyField(\result.get(0), \result.get(1), board.calculateY(\result.get(0), \result.get(1)));
	 */
	/**
	 * Asks the player where to put the mark in the field using input and output. 
	 *
	 * @param board the board
	 * @return The field the player has chosen
	 */
	
	public ArrayList<Integer> determineMove(Board board){
		String prompt = "> " + getName() + " (" + getMark().toString() + ")"
				+ ", what is your choice? ";
		System.out.print(prompt);
		int choicex = -1;
		int choicez = -1;
		Scanner line = new Scanner(System.in);
		String coordinates = line.nextLine();
		String[] ar = coordinates.split(",");
		choicex = Integer.parseInt(ar[0]);
		choicez = Integer.parseInt(ar[1]);
		boolean valid = board.isField(choicex, choicez, board.calculateY(choicex, choicez)) && board.isEmptyField(choicex, choicez, board.calculateY(choicex, choicez));
		while (!valid) {
			System.out.println("ERROR: field " + "(" + choicex + "," + choicez + "," +  board.calculateY(choicex, choicez)+ ")" + " is no valid choice.");
			System.out.println(prompt);
			Scanner line3 = new Scanner(System.in);
			String coordinates3 = line3.nextLine();
			String[] ar3 = coordinates3.split(",");
			choicex = Integer.parseInt(ar3[0]);
			choicez = Integer.parseInt(ar3[1]);
			valid = board.isField(choicex, choicez, board.calculateY(choicex, choicez)) && board.isEmptyField(choicex, choicez, board.calculateY(choicex, choicez));
		}
		
		
		ArrayList<Integer> XandZ = new ArrayList<Integer>();
		XandZ.add(choicex);
		XandZ.add(choicez);
		return XandZ;
	}	

}
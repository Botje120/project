package game;

/**
 * The Enum Mark.
 */
public enum Mark {
	
	/** The empty. */
	EMPTY, 
 /** The red. */
 RED, 
 /** The yellow. */
 YELLOW;



	/*@
	    ensures this == Mark.RED ==> \result == Mark.YELLOW;
	    ensures this == Mark.YELLOW ==> \result == Mark.RED;
	    ensures this == Mark.EMPTY ==> \result == Mark.EMPTY;
	 */
	/**
	 * Returns the others Mark.
	 *
	 * @return the Mark2 of the other player
	 */
	public Mark other() {
		if (this == Mark.RED) {
			return Mark.YELLOW;
		} else if (this == Mark.YELLOW) {
			return Mark.RED;
		} else {
			return Mark.EMPTY;
		}
	}
}



package game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * The Class NaiveStrategy.
 */
public class NaiveStrategy implements Strategy{
	
	/** The name. */
	private String name = "Naive";
	
	/**
	 * Get the name of the player.
	 * @return the name of the player.
	 */
	//@pure
	public String getName(){
		return name;
	}
	
	/**
	 * Determines the move of the player.
	 *
	 * @param b 	the board the player is playing on.
	 * @param m 	the mark the player is playing with.
	 * @return 	an ArrayList of the coordinates of the point which will be marked.
	 */
	/*@
 		requires b != null;
 		requires (m == Mark.RED || m == Mark.YELLOW);
 		requires !b.isFull();
	 */
	public ArrayList<Integer> determineMove(Board b, Mark m){
		Set<ArrayList<Integer>> free = new HashSet<ArrayList<Integer>>();
		ArrayList<Integer> XandZandY = new ArrayList<Integer>();
		for (int x = 0; x < 4; x++){
			for (int z = 0; z < 4; z++){
				for (int y= 0; y < 4; y++){
					if (b.isEmptyField(x, z, y)){
						XandZandY.add(0, x);
						XandZandY.add(1, z);
						XandZandY.add(2, y);
						free.add(XandZandY);
						XandZandY.clear();
					}
				}
			}
		}



		ArrayList<Integer> result = null;
		int size = free.size();
		int i = 0;
		int stop = (int)(Math.random() * size);
		System.out.println("Random position:" + stop + ", Output:");
		for (ArrayList<Integer> it : free){
			if (i == stop){
				result = it;
				break;
			}
			i++;
		}
		System.out.println(result);
		return result;
	}
}
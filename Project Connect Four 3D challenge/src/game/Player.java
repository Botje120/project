package game;

import java.util.ArrayList;
import java.util.Observer;

/**
 * The Class Player.
 */
public abstract class Player{

	/** The name. */
	private String name;
	
	/** The mark. */
	private Mark mark;
	/*@
    	requires name != null;
    	requires mark == Mark.RED || mark== Mark.YELLOW;
    	ensures this.getName() == name;
    	ensures this.getMark() == mark;
	 */
	/**
	 * Creates a new Player object.
	 *
	 * @param name the name
	 * @param mark the mark
	 */
	public Player(String name, Mark mark){
		this.name = name;
		this.mark = mark; 
	}


	/**
	 * Get the name of the player.
	 * @return the name of the player.
	 */
	 /*@ pure */
	public String getName(){
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param nickname the new name
	 */
	public void setName(String nickname){
		name = nickname;
	}

	/**
	 * Get the mark of the player.  
	 * @return the mark of the player.
	 */
	 /*@ pure */
	public Mark getMark(){
		return mark; 
	}
	
	/**
	 * Sets the mark.
	 *
	 * @param m the new mark
	 */
	public void setMark(Mark m){
		mark = m;
	}

	/*@
    	requires board != null & !board.isFull();
    	ensures board.isField(\result.get(0), \result.get(1), board.calculateY(\result.get(0), \result.get(1))) & board.isEmptyField(\result.get(0), \result.get(1), board.calculateY(\result.get(0), \result.get(1)));
	 */
	/**
	 * Determines the field for the next move. 
	 *
	 * @param board the board
	 * @return the player's choice
	 */
	public abstract ArrayList<Integer> determineMove(Board board);

	/**
	 * Makes a move on the board.
	 *
	 * @param board the board
	 * @throws ArrayIndexOutOfBoundsException the array index out of bounds exception
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	/*@
    	requires board != null & !board.isFull();
	 */
	public void makeMove(Board board) throws ArrayIndexOutOfBoundsException, IndexOutOfBoundsException {
		try{
			ArrayList<Integer> determineMove = new ArrayList<Integer>();
			determineMove = determineMove(board);
			int choicex = determineMove.get(0);
			int choicez = determineMove.get(1);
			int choicey = board.calculateY(choicex, choicez);
			board.setField(choicex, choicez, choicey, getMark());
		}
		catch (ArrayIndexOutOfBoundsException e){
			System.out.println("ERROR: this field is no valid choice.");
			makeMove(board);
		}
		catch (IndexOutOfBoundsException f){
			System.out.println(f.getMessage());
		}
	}

	/**
	 * Make move.
	 *
	 * @param board the board
	 * @param x the x
	 * @param z the z
	 * @param y the y
	 */
	public void makeMove(Board board, int x, int z, int y){
		board.setField(x, z, y, getMark());
	}


}
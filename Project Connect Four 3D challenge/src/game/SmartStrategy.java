package game;

import java.util.*;

/**
 * The Class SmartStrategy.
 */
public class SmartStrategy implements Strategy{
	
	/** The name. */
	String name = "smart";
	/**
	 * Get the name of the player.
	 * @return the name of the player.
	 */
	//@pure
	@Override
	public String getName() {
		return name;
	}
	
	/**
	 * Determines the move of the player.
	 *
	 * @param b 	the board the player is playing on.
	 * @param m 	the mark the player is playing with.
	 * @return 	an ArrayList of the coordinates of the point which will be marked.
	 */
	/*@
 	requires b != null;
 	requires (m == Mark.RED || m == Mark.YELLOW);
 	requires !b.isFull();
	 */
	@Override
	public ArrayList<Integer> determineMove(Board b, Mark m) {
		ArrayList<Integer> xAndzAndy1 = new ArrayList<Integer>();
		for (int x = 0; x < 4; x++){
			for (int z = 0; z < 4; z++){
				for (int y= 0; y < 4; y++){
					Board boardcopy = b.deepCopy();
					if (boardcopy.isEmptyField(x, z, y)){
						boardcopy.setField(x, z, y, m);
						if (boardcopy.isWinner(m)){
							xAndzAndy1.add(0, x);
							xAndzAndy1.add(1, z);
							xAndzAndy1.add(2, y);
							return xAndzAndy1;
						}
					}
				}
			}
		}
		
		for (int x = 0; x < 4; x++){
			for (int z = 0; z < 4; z++){
				for (int y= 0; y < 4; y++){
					Board boardcopy = b.deepCopy();
					if (boardcopy.isEmptyField(x,z,y)){
						boardcopy.setField(x, z, y, m.other());
						if (boardcopy.isWinner(m.other())){
							xAndzAndy1.add(0, x);
							xAndzAndy1.add(1, z);
							xAndzAndy1.add(2, y);
							return xAndzAndy1;
						}
					}
				}
			}
		}

		Set<ArrayList<Integer>> free = new HashSet<ArrayList<Integer>>();
		
		for (int x = 0; x < 4; x++){
			for (int z = 0; z < 4; z++){
				for (int y= 0; y < 4; y++){
					if (b.isEmptyField(x, z, y)){
						ArrayList<Integer> xAndzAndy2 = new ArrayList<Integer>();
						xAndzAndy2.add(0, x);
						xAndzAndy2.add(1, z);
						xAndzAndy2.add(2, y);
						free.add(xAndzAndy2);
					}
				}
			}
		}



		ArrayList<Integer> result = null;
		int size = free.size();
		int i = 0;
		int stop = (int)(Math.random() * size);
		System.out.println("Random posistion:" + stop + ", Output:");
		for (ArrayList<Integer> it : free){
			if (i == stop){
				result = it;
			}
			i++;
		}
		System.out.println(result);
		return result;

	}
}
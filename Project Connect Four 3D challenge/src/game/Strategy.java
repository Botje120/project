package game;

import java.util.ArrayList;

/**
 * The Interface Strategy.
 */
public interface Strategy {
	/**
	 * Get the name of the player.
	 * @return the name of the player.
	 */
	//@pure
	public String getName();
	
	/**
	 * Determines the move of the player.
	 *
	 * @param b 	the board the player is playing on.
	 * @param m 	the mark the player is playing with.
	 * @return 	an ArrayList of the coordinates of the point which will be marked.
	 */
	/*@
	 	requires b != null;
	 	requires (m == Mark.RED || m == Mark.YELLOW);
	 	requires !b.isFull();
	 */
	public ArrayList<Integer> determineMove(Board b, Mark m);
}
